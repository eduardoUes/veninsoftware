package veninsoftware;
import claseConectar.conectar;
import java.io.File;
import java.io.FileInputStream;
import java.sql.*;
import javax.swing.JOptionPane;

public class SQLfunciones {
    private Statement sentencia;
    public int t_u=0;
    conectar cc= new conectar();
    Connection cn= cc.conexion();
    
    public void Insertar(String Tabla,String Campos){
        try{
            sentencia=cn.createStatement();
            String query = "INSERT INTO "+Tabla+" VALUES ( "+Campos+" ) ";
            JOptionPane.showMessageDialog(null,query);
            sentencia.executeUpdate(query);
            JOptionPane.showMessageDialog(null,"Se Inserto correctamente");
            cn.close() ;
        } //fin bloque try
        catch(SQLException e){
            JOptionPane.showMessageDialog(null, "Error 1 " + e.getMessage());
        }
    }//fin método Insertar

    public void Insert(Integer cant,String det,String pv,String ruta){
        try{
            FileInputStream fis = null;
            File file = new File(ruta);
            fis = new FileInputStream(file);      
            String sql = "insert into tb_producto(id_producto,cantidad,detalle,precio_v,imagen) values(?,?,?,?,?)";
            PreparedStatement pst  = cn.prepareStatement(sql);   
            pst.setString(1,"");
            pst.setInt(2,cant);
            pst.setString(3,det);
            pst.setString(4,pv);
            pst.setBinaryStream(5,fis,(int)file.length()); 
            int n=pst.executeUpdate();
            if(n>0)
            JOptionPane.showMessageDialog(null, "Registro Guardado con Exito");  
            pst.close();
            fis.close();
        } //fin bloque try
        catch(Exception e){
            JOptionPane.showMessageDialog(null, "Error 1 " + e.getMessage());
        }
    }
    
    public void update(String id,Integer cant,String detall,String pv,String ruta){
        FileInputStream fis = null;
        File file = new File(ruta);
        String query = "update tb_producto set cantidad = ?, detalle = ? ,precio_v = ?, imagen = ? where id_producto = ?";
        try{
            fis = new FileInputStream(file);
            PreparedStatement pst = cn.prepareStatement(query);
            pst.setInt(1,cant);
            pst.setString(2,detall);
            pst.setString(3,pv);
            pst.setBinaryStream(4,fis,(int)file.length());
            pst.setString(5,id);
            int n=pst.executeUpdate();
            if(n>0)
                JOptionPane.showMessageDialog(null, "Registro Guardado con Exito");  
            pst.close();
            fis.close();
            cn.close();
        }
        catch(Exception e){
            JOptionPane.showMessageDialog(null, "Error 1 " + e.getMessage());
        }
    }
    
    public void Actualizar(String Tabla,String Campos, String Id){
        try{
            sentencia=cn.createStatement();
            String sql_str = "UPDATE "+Tabla+" SET "+Campos+" WHERE "+Id+" ";
            //JOptionPane.showMessageDialog(null,sql_str);
            sentencia.executeUpdate(sql_str);
            JOptionPane.showMessageDialog(null,"Se Modifico correctamente");
            cn.close();
        } //fin bloque try
        catch(SQLException e) {
            JOptionPane.showMessageDialog(null, "Error 1 " + e.getMessage());
        }
    }//fin método Actualizar
    
    public void Eliminar(String Tabla,String Id){
        try{
            sentencia=cn.createStatement();
            String sql_str = "DELETE FROM "+Tabla+" WHERE "+Id+" ";
            sentencia.executeUpdate(sql_str);
            //JOptionPane.showMessageDialog(null,sql_str);
            JOptionPane.showMessageDialog(null,"Se Elimino correctamente");
            cn.close() ;
        } // fin bloque try
        catch(SQLException e) {
            JOptionPane.showMessageDialog(null, "Error 1 " + e.getMessage());
        }
    }//fin método Eliminar
    
    public int validUser(String nom, String contra){
        int tipo =0;
        try{
            String query="Select * from tb_users where usuario='"+nom+"' and password=password('"+contra+"')";
            //JOptionPane.showMessageDialog(null,query);
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(query);
            while(rs.next()){
                tipo = rs.getInt("rol");
            }
        } //fin bloque try
        catch(SQLException e){
            JOptionPane.showMessageDialog(null, "Error 1 " + e.getMessage());
        }    
        return tipo;
    }
    
}
