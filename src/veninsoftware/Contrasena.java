/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package veninsoftware;
import claseConectar.conectar;
import java.sql.*;
import java.util.logging.*;
import javax.swing.JOptionPane;

/**
 *
 * @author Eduardo
 */
public class Contrasena extends javax.swing.JInternalFrame {

    /**
     * Creates new form Contrasena
     */
    public Contrasena() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel4 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        repnuevtxt = new javax.swing.JPasswordField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        nuevtxt = new javax.swing.JPasswordField();
        viejatxt = new javax.swing.JPasswordField();
        jLabel3 = new javax.swing.JLabel();
        btncontra = new javax.swing.JButton();

        jLabel4.setText("jLabel4");

        setClosable(true);
        setIconifiable(true);
        setTitle("Cambiar Contrasena");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Nueva contrasena", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 12), new java.awt.Color(0, 51, 204))); // NOI18N

        repnuevtxt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                repnuevtxtKeyTyped(evt);
            }
        });

        jLabel1.setText("Contrasena Antigua");

        jLabel2.setText("Contrasena Nueva");

        nuevtxt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                nuevtxtKeyTyped(evt);
            }
        });

        viejatxt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                viejatxtKeyTyped(evt);
            }
        });

        jLabel3.setText("Repetir Contrasena");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3))
                .addGap(37, 37, 37)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(nuevtxt, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(viejatxt, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(repnuevtxt, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(viejatxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(nuevtxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(repnuevtxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btncontra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/veninsoftware/Imagen/Lock.png"))); // NOI18N
        btncontra.setText("Cambiar Contrasena");
        btncontra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncontraActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btncontra)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btncontra)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btncontraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncontraActionPerformed
        // TODO add your handling code here:
        String buscar="select * from tb_users where password=password('"+viejatxt.getText()+"') AND usuario='"+Login.usuario+"'";
        //JOptionPane.showMessageDialog(null, buscar);
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(buscar);
            if(rs.next()){
                String id= rs.getString("id");
                if(nuevtxt.getText().equals(repnuevtxt.getText())){
                    String query="password=password('"+nuevtxt.getText()+"')";
                    SQLfunciones act=new SQLfunciones();
                    act.Actualizar("tb_users",query,"id ='"+id+"'");
                    this.limpiarContra();
                }
                else{
                    JOptionPane.showMessageDialog(null, "La Contrasena no coincide");
                    this.limpiarContra();
                }
            }
            else{
                JOptionPane.showMessageDialog(null, "La Contrasena vieja es erronea");
                this.limpiarContra();
            }
        } catch (SQLException ex) {
            Logger.getLogger(Usuarios.class.getName()).log(Level.SEVERE, null, ex);
        }   
    }//GEN-LAST:event_btncontraActionPerformed

    private void viejatxtKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_viejatxtKeyTyped
        // TODO add your handling code here:
        char car = evt.getKeyChar();
        if((car<'a' || car>'z') &&(car<'A' || car>'Z')&&(car<'0' || car>'9')) evt.consume();
    }//GEN-LAST:event_viejatxtKeyTyped

    private void nuevtxtKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_nuevtxtKeyTyped
        // TODO add your handling code here:
        char car = evt.getKeyChar();
        if((car<'a' || car>'z') &&(car<'A' || car>'Z')&&(car<'0' || car>'9')) evt.consume();
    }//GEN-LAST:event_nuevtxtKeyTyped

    private void repnuevtxtKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_repnuevtxtKeyTyped
        // TODO add your handling code here:
        char car = evt.getKeyChar();
        if((car<'a' || car>'z') &&(car<'A' || car>'Z')&&(car<'0' || car>'9')) evt.consume();
    }//GEN-LAST:event_repnuevtxtKeyTyped

public void limpiarContra(){
    this.viejatxt.setText("");
    this.nuevtxt.setText("");
    this.repnuevtxt.setText("");
}
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btncontra;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPasswordField nuevtxt;
    private javax.swing.JPasswordField repnuevtxt;
    private javax.swing.JPasswordField viejatxt;
    // End of variables declaration//GEN-END:variables
    conectar cc= new conectar();
    Connection cn= cc.conexion();
}
